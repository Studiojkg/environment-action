# Use this pipeline to publish a message to a Slack channel about the status of Uffizzi environment.
# Requirements:
#  - Create a Slack app and connect it to your workspace
#  - Create a Slack incoming webhook URL
#  - Add the following variables to your project:
#    - SLACK_WEBHOOK_URL (Slack incoming webhook URL)
#    - SLACK_CHANNEL (Slack channel name)
#    - SLACK_TOKEN (Slack app token)

notification_on_deploy:
  stage: uffizzi_deploy
  image: $UFFIZZI_CURL_IMAGE
  needs:
    - healthcheck_environment
    - deploy_environment
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH
  before_script: []
  script:
    - echo "Posting notification to Slack"
    - |
      curl --location --request POST "${SLACK_WEBHOOK_URL}" \
      --header 'Content-Type: application/json' \
      --header "Authorization: Bearer ${SLACK_TOKEN}" \
      -d @- <<EOF
      { "blocks": [ { "type": "section","text": { "type": "mrkdwn","text": ":white_check_mark:Uffizzi Environment ${ACTION} at URL:\n<$UFFIZZI_PROXY_URL>\nUffizzi Environment deployment details at URI:\n<${UFFIZZI_CONTAINERS_URI}>\nUffizzi Environment ID: *${UFFIZZI_ENVIRONMENT_ID}*\nMerge request: <$CI_MERGE_REQUEST_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_IID|$CI_PROJECT_NAME-MR-$CI_MERGE_REQUEST_IID>" } } ] }
      EOF
    - echo "UFFIZZI_PROXY_URL=$UFFIZZI_PROXY_URL" >> UFFIZZI_DEPLOY_ENV_FILE
  artifacts:
    reports:
      dotenv: UFFIZZI_DEPLOY_ENV_FILE
  environment:
    name: "uffizzi/MR-${CI_MERGE_REQUEST_IID}"
    url: $UFFIZZI_PROXY_URL
    on_stop: notification_on_stop
    action: access

notification_on_stop:
  stage: uffizzi_delete
  image: $UFFIZZI_CURL_IMAGE
  needs:
    - delete_environment
    - deploy_environment
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH
  before_script: []
  script:
    - echo "Posting notification to Slack"
    - |
      curl --location --request POST "${SLACK_WEBHOOK_URL}" \
      --header 'Content-Type: application/json' \
      --header "Authorization: Bearer ${SLACK_TOKEN}" \
      -d @- <<EOF
      {"blocks":[{"type":"section","text":{"type":"mrkdwn","text":":warning:Uffizzi Environment *${UFFIZZI_ENVIRONMENT_ID}* deleted\nMerge request:\n<$CI_MERGE_REQUEST_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_IID|$CI_PROJECT_NAME-MR-$CI_MERGE_REQUEST_IID>"}}]}
      EOF
  environment:
    name: "uffizzi/MR-${CI_MERGE_REQUEST_IID}"
    action: stop

notification_on_failure:
  stage: uffizzi_deploy
  image: $UFFIZZI_CURL_IMAGE
  needs:
    - deploy_environment
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $CI_DEFAULT_BRANCH
  before_script: []
  script:
    - echo "Posting notification to Slack"
    - |
      curl --location --request POST "${SLACK_WEBHOOK_URL}" \
      --header 'Content-Type: application/json' \
      --header "Authorization: Bearer ${SLACK_TOKEN}" \
      -d @- <<EOF
      {"blocks":[{"type":"section","text":{"type":"mrkdwn","text":":exclamation:Failed Uffizzi Environment deploy in Merge request:\n<$CI_MERGE_REQUEST_PROJECT_URL/-/merge_requests/$CI_MERGE_REQUEST_IID|$CI_PROJECT_NAME-MR-$CI_MERGE_REQUEST_IID>\nPipeline:\n${CI_PIPELINE_URL}"}}]}
      EOF
  when: on_failure
  environment:
    name: "uffizzi/MR-${CI_MERGE_REQUEST_IID}"
